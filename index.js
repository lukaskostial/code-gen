const toSizes = (words) => words.map(w => w.length)

// Just helper to map one iterable to another
function* map(iterable, mapFunc) {
  for (let x of iterable) {
    yield mapFunc(x)
  }
}

// Generates values from max down to min
function* gen(min, max) {
  if(max >= min) {
    yield max
    if(max > min) {
      yield* gen(min, max - 1)
    }
  }
}

// generates a list of subsizes of the words to form a string of specified length
function *genList(sizes, length) {
  if(sizes.length === 1) {
    if(sizes[0] >= length) {
      yield* map(gen(length, length), (l) => [l])
    }
  } else {
    for(let x of gen(1, Math.min(sizes[0], length - sizes.length + 1))) {
      const [first, ...tail] = sizes
      yield* map(genList(tail, length - x), (r) => [x, ...r])
    }
  }
}

// generates a list of subsizes of the words to form a string of specified length or shorter (till min)
function* genDescendingList(sizes, min, max) {
  for(let x of gen(min, max)) {
    yield* genList(sizes, x)
  }
}

// generates code from company name with length belonging to (min, max) interval
function* generate(name, min, max) {
  const words = name.split(' ')
  const sizes = toSizes(words)
  yield* map(genDescendingList(sizes, min, max), (list) => {
    return list.map((size, index) => words[index].substring(0, size).toUpperCase()).join('')
  })
}


for(let x of generate('lukoil trading company', 3, 6)) {
  console.log(x)
}
